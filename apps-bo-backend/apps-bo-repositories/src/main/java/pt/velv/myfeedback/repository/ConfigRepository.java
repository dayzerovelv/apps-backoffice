package pt.velv.myfeedback.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.velv.apps.model.config.Configuration;

@Repository("myf.configRepo")
public interface ConfigRepository extends MongoRepository<Configuration, String> {
}
