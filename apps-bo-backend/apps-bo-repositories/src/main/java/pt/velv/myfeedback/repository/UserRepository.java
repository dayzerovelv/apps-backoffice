package pt.velv.myfeedback.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.velv.apps.model.user.User;

import java.util.List;

@Repository("myf.userRepo")
public interface UserRepository extends MongoRepository<User, String> {

    List<User> findByNotificationTokenIsNotNull();
}
