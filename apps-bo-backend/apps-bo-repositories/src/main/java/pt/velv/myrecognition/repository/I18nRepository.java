package pt.velv.myrecognition.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.velv.apps.model.i18n.I18NData;

@Repository("myr.i18nRepo")
public interface I18nRepository extends MongoRepository<I18NData, String> {
}
