package pt.velv.myfeedback.controller.user;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.velv.apps.services.user.UserService;

@RestController
@RequestMapping("/myf/user")
public class UserController {

    private static final Log LOG = LogFactory.getLog(UserController.class);

    @Autowired
    @Qualifier("myf.userService")
    private UserService userService;

    @GetMapping("/token")
    public ResponseEntity<?> getAllUsersWithNotificationToken(){
        try{
            return ResponseEntity.ok().body(this.userService.getAllUsersWithNotificationToken());
        }catch (Exception e){
            LOG.error("ERROR GETTING USERS WITH NOTIF TOKENS", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao obter utilizadores");
        }
    }

}
