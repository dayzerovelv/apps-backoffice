package pt.velv.myfeedback.controller.configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pt.velv.apps.model.config.Configuration;
import pt.velv.apps.services.config.ConfigService;
import pt.velv.apps.services.i18n.I81nService;

@RestController
@RequestMapping("/myf/config")
public class ConfigurationController {

    private static final Log LOG = LogFactory.getLog(ConfigurationController.class);

    @Autowired
    @Qualifier("myf.I18nService")
    private I81nService i18nService;

    @Autowired
    @Qualifier("myf.configService")
    private ConfigService configService;

    @PostMapping("/i18n")
    public ResponseEntity<?> uploadI18nFile(@RequestParam("i18nFile") MultipartFile file
            , @RequestParam("userId") String userId) {

        try{
            i18nService.loadTranslationsFile(file.getInputStream(), userId);
            return ResponseEntity.ok().body("Ficheiro carregado com sucesso");
        }catch(Exception e){
            LOG.error("ERROR PARSING FILE", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao carregar ficheiro");
        }
    }

    @GetMapping("/i18n/langs")
    public ResponseEntity<?> getSupportedLangs(){
        try{
            return ResponseEntity.ok().body(i18nService.getSupportedLangs());
        }catch (Exception e){
            LOG.error("ERROR LOADING SUPPORTED LANGS", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao carregar línguas suportadas");
        }
    }

    @GetMapping
    public ResponseEntity<?> getConfigurations(){
        try{
            return ResponseEntity.ok(this.configService.getConfiguration());
        }catch (Exception e){
            LOG.error("ERROR GETTING CONFIGS", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao obter configurações");
        }
    }

    @PostMapping
    public ResponseEntity<?> applyConfigurations(@RequestBody Configuration configuration){
        try{
            configService.applyConfigurations(configuration);
            return ResponseEntity.ok().body("Configurações aplicadas com sucesso");
        }catch(Exception e){
            LOG.error("ERROR APPLYING CONFIGS", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao aplicar configurações");
        }
    }
}
