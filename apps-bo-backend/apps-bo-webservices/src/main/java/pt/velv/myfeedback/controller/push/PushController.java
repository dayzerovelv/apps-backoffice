package pt.velv.myfeedback.controller.push;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.velv.apps.model.notification.PushMessage;
import pt.velv.apps.services.push.PushService;

import java.util.List;

@RestController
@RequestMapping("/myf/push")
public class PushController {

    private static final Log LOG = LogFactory.getLog(PushController.class);

    @Autowired
    @Qualifier("myf.pushService")
    private PushService pushService;

    @PostMapping("/single")
    public ResponseEntity<?> sendSingleNotification(@RequestBody PushMessage message){
        try {
            this.pushService.sendNotification(message.getTitle(), message.getMessage(), message.getToken());
            return ResponseEntity.ok().body("Notificação enviada com sucesso");
        }catch (Exception e){
            LOG.error("ERROR SENDING SINGLE NOTIFICATION", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao enviar notificação");
        }
    }

    @PostMapping("/all")
    public ResponseEntity<?> sendMultipleNotifications(@RequestBody List<PushMessage> messages){
        try {
            for(PushMessage message: messages) {
                this.pushService.sendNotification(message.getTitle(), message.getMessage(), message.getToken());
            }
            return ResponseEntity.ok().body("Notificação enviada com sucesso");
        }catch (Exception e){
            LOG.error("ERROR SENDING SINGLE NOTIFICATION", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erro ao enviar notificação");
        }
    }


}
