package pt.velv.myfeedback.services.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pt.velv.apps.model.config.Configuration;
import pt.velv.apps.services.config.ConfigService;
import pt.velv.myfeedback.repository.ConfigRepository;

import java.util.List;

@Service(value = "myf.configService")
public class ConfigServiceImpl implements ConfigService {

    private static final Log LOG = LogFactory.getLog(ConfigServiceImpl.class);

    @Autowired
    @Qualifier("myf.configRepo")
    private ConfigRepository repository;

    @Override
    public void applyConfigurations(Configuration configuration) {
        LOG.info("Applying Configuration\n" + configuration.toString());
        this.repository.save(configuration);
    }

    @Override
    public Configuration getConfiguration() throws Exception {
        List<Configuration> configurations = this.repository.findAll();
        if (configurations != null && !configurations.isEmpty()) {
            return configurations.get(0);
        }
        throw new Exception();
    }
}
