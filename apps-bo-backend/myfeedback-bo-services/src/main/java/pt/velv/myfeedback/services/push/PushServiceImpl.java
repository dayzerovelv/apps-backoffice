package pt.velv.myfeedback.services.push;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.velv.apps.services.push.PushService;
import pt.velv.firebase.model.notification.NotificationRequest;
import pt.velv.firebase.model.notification.NotificationType;
import pt.velv.firebase.model.response.FirebaseResponse;
import pt.velv.firebase.services.notification.NotificationService;

@Service("myf.pushService")
public class PushServiceImpl implements PushService {

    private static final Log LOG = LogFactory.getLog(PushServiceImpl.class);

    @Autowired
    private NotificationService notificationService;

    @Override
    public String sendNotification(String title, String messageToSend, String toDevice) {

        NotificationRequest request = new NotificationRequest();
        request.setMessageToSend(messageToSend);
        request.setNotificationToken(toDevice);
        request.setTitle(title);
        request.setNotificationType(NotificationType.BACKOFFICE);
        FirebaseResponse firebaseResponse = notificationService.sendNotification(request);
        return firebaseResponse.toString();
    }
}
