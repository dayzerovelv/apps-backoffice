package pt.velv.myfeedback.services.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pt.velv.apps.model.user.User;
import pt.velv.apps.services.user.UserService;
import pt.velv.myfeedback.repository.UserRepository;

import java.util.List;

@Service(value = "myf.userService")
public class UserServiceImpl implements UserService {

	@Autowired
	@Qualifier("myf.userRepo")
	private UserRepository userRepository;


	@Override
	public List<User> getAllUsersWithNotificationToken() {
		return this.userRepository.findByNotificationTokenIsNotNull();
	}
}
