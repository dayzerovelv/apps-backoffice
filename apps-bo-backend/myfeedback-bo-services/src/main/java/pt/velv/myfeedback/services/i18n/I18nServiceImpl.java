package pt.velv.myfeedback.services.i18n;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pt.velv.apps.model.i18n.I18NData;
import pt.velv.apps.model.i18n.Translation;
import pt.velv.apps.services.i18n.I81nService;
import pt.velv.myfeedback.repository.I18nRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service(value = "myf.I18nService")
public class I18nServiceImpl implements I81nService {

    private static final Log LOG = LogFactory.getLog(I18nServiceImpl.class);

    @Autowired
    @Qualifier("myf.i18nRepo")
    private I18nRepository repository;

    @Override
    public void loadTranslationsFile(InputStream inputStream, String userId) throws IOException {
        LOG.info("USER " + userId + " UPLOADING I18N FILE");

        ObjectMapper jsonMapper = new ObjectMapper();
        jsonMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        List<I18NData> i18NDataList = jsonMapper.readValue(inputStream, new TypeReference<List<I18NData>>() {
        });
        this.repository.saveAll(i18NDataList);
    }

    @Override
    public Map<String, String> getSupportedLangs() {
        List<I18NData> i18NData = this.repository.findAll();
        Map<String, String> supportedLangs = new HashMap();

        if(i18NData != null && !i18NData.isEmpty()){
            List<Translation> translations = i18NData.get(0).getTranslations();
            for(Translation translation : translations){
                supportedLangs.put(translation.getLocale()+"", translation.getLocale().getDisplayLanguage());
            }
        }
        return supportedLangs;
    }
}
