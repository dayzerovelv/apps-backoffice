package pt.velv.apps.model.user;

public enum UserStatus {
	ACTIVE, INACTIVE, PENDING_ACTIVATION, FIRST_LOGIN
}
