package pt.velv.apps.model.i18n;

import java.io.Serializable;
import java.util.Locale;

public class Translation implements Serializable {

	private static final long serialVersionUID = 6789289294665815485L;

	private Locale locale;
	private String value;


	public Locale getLocale() {
		return locale;
	}

	public void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}
}
