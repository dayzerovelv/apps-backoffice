package pt.velv.apps.model.config;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.io.Serializable;

@Entity
public class Configuration implements Serializable {

    private static final long serialVersionUID = -3226512925305295763L;

    @Id
    private String id;

    private String defaultLang;
    private String bgColor;
    private boolean dailyMood;
    private boolean categories;
    private boolean gamification;
    private boolean recursiveFeedback;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDefaultLang() {
        return defaultLang;
    }

    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public boolean isDailyMood() {
        return dailyMood;
    }

    public void setDailyMood(boolean dailyMood) {
        this.dailyMood = dailyMood;
    }

    public boolean isCategories() {
        return categories;
    }

    public void setCategories(boolean categories) {
        this.categories = categories;
    }

    public boolean isGamification() {
        return gamification;
    }

    public void setGamification(boolean gamification) {
        this.gamification = gamification;
    }

    public boolean isRecursiveFeedback() {
        return recursiveFeedback;
    }

    public void setRecursiveFeedback(boolean recursiveFeedback) {
        this.recursiveFeedback = recursiveFeedback;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "id='" + id + '\'' +
                ", defaultLang='" + defaultLang + '\'' +
                ", bgColor='" + bgColor + '\'' +
                ", dailyMood=" + dailyMood +
                ", categories=" + categories +
                ", gamification=" + gamification +
                ", recursiveFeedback=" + recursiveFeedback +
                '}';
    }
}
