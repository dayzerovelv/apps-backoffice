package pt.velv.apps.model.notification;

import java.io.Serializable;

public class PushMessage implements Serializable {

    private static final long serialVersionUID = 8345741604923984725L;

    private String title;
    private String message;
    private String token;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
