package pt.velv.apps.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import java.io.Serializable;

@Entity
public class User implements Serializable {

	private static final long serialVersionUID = -2276462532059583356L;

	@Id
	private String id;

	private String name;

	private String surname;

	private String email;

	private String password;

	private String department;

	private String teamName;

	private String companyRole;

	private String image;

	private int credits;

	@JsonIgnore
	private String signUpCode;

	@Property("status")
	private UserStatus status;

	@JsonIgnore
	private String deviceId;

	private String notificationToken;

	@JsonIgnore
	private String accessLevels;


	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(final String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(final String department) {
		this.department = department;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public String getSignUpCode() {
		return signUpCode;
	}

	public void setSignUpCode(final String signUpCode) {
		this.signUpCode = signUpCode;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(final int credits) {
		this.credits = credits;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(final UserStatus status) {
		this.status = status;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(final String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(final String teamName) {
		this.teamName = teamName;
	}

	public String getCompanyRole() {
		return companyRole;
	}

	public void setCompanyRole(final String companyRole) {
		this.companyRole = companyRole;
	}

	public String getAccessLevels() {
		return accessLevels;
	}

	public void setAccessLevels(final String accessLevels) {
		this.accessLevels = accessLevels;
	}

	public String getImage() {
		return image;
	}

	public void setImage(final String image) {
		this.image = image;
	}

	public String getNotificationToken() {
		return notificationToken;
	}

	public void setNotificationToken(final String notificationToken) {
		this.notificationToken = notificationToken;
	}
}
