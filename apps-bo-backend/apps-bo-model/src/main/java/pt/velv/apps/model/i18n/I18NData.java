package pt.velv.apps.model.i18n;

import org.mongodb.morphia.annotations.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class I18NData implements Serializable {

	private static final long serialVersionUID = -9204468091492901345L;

	private String key;
	private List<Translation> translations = new ArrayList<Translation>();

	public String getKey() {
		return key;
	}

	public void setKey(final String key) {
		this.key = key;
	}

	public List<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(final List<Translation> translations) {
		this.translations = translations;
	}

	public void addTranslation(final Translation translation) {
		translations.add(translation);
	}
}
