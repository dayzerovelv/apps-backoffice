package pt.velv.apps.services.config;

import pt.velv.apps.model.config.Configuration;

public interface ConfigService {

    void applyConfigurations(Configuration configuration);

    Configuration getConfiguration() throws Exception;
}
