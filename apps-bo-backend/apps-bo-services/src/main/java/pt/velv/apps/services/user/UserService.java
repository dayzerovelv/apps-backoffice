package pt.velv.apps.services.user;

import pt.velv.apps.model.user.User;

import java.util.List;

public interface UserService {

	List<User> getAllUsersWithNotificationToken();
}
