package pt.velv.apps.services.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface I81nService {

    void loadTranslationsFile(InputStream inputStream, String userId) throws IOException;

    Map<String, String> getSupportedLangs();
}
