package pt.velv.apps.services.push;

public interface PushService {

	String sendNotification(String title, String messageToSend, String toDevice);
}
