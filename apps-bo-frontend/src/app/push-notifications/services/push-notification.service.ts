import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';
import { PushMessage } from '../interfaces/push-message';

@Injectable()
export class PushNotificationService {

  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  sendSingleNotification(message: PushMessage) {
    return this.http.post(environment.pushSingleURL, JSON.stringify(message), { headers: this.headers });
  }

  sendAll(messages: PushMessage[]) {
    return this.http.post(environment.pushAllURL, JSON.stringify(messages), { headers: this.headers });
  }
}
