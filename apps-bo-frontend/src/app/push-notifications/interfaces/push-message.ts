export class PushMessage {
  title: string;
  message: string;
  token: string;

  constructor(){
    this.title = '';
    this.message = '';
    this.token = '';
  }
}
