import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { LoaderService } from '../../shared/services/loader.service';
import { User } from '../../users/interfaces/user';
import { UserService } from '../../users/services/user.service';
import { PushMessage } from '../interfaces/push-message';
import { PushNotificationService } from '../services/push-notification.service';
import { MatSnackBar } from '@angular/material';

@Component({
  templateUrl: './push-notification.component.html',
  styleUrls: ['./push-notification.component.scss']
})
export class PushNotificationComponent implements OnInit {

  users: User[];
  pushMessage: PushMessage;
  selectedUserToken: string;
  sendAll = new FormControl(false);

  constructor(private userService: UserService,
    private pushService: PushNotificationService,
    private loader: LoaderService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.pushMessage = new PushMessage();
    this.loader.display(true);
    this.userService.getUsersWithNotificationToken()
      .subscribe((users) => {
        this.users = users;
        this.loader.display(false);
      });
  }

  send() {
    this.loader.display(true);
    if (this.sendAll.value) {
      this.loader.display(false);
      let messages: PushMessage[] = [];
      this.users.forEach(user => {
        let message: PushMessage = new PushMessage();
        message.token = user.notificationToken;
        message.title = this.pushMessage.title;
        message.message = this.pushMessage.message;
        messages.push(message);
      });
      this.pushService.sendAll(messages)
        .subscribe(() => {
          this.snackBar.open('Mensagens Push Enviadas com Sucesso', 'OK', { duration: 2000, });
          this.loader.display(false);
        });
    }
    else {
      this.pushMessage.token = this.selectedUserToken;
      this.pushService.sendSingleNotification(this.pushMessage)
        .subscribe(() => {
          this.snackBar.open('Mensagem Push Enviada com Sucesso', 'OK', { duration: 2000, });
          this.loader.display(false);
        });
    }
  }
}
