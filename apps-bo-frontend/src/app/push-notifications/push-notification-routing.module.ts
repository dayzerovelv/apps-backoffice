import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Route } from '../shared/route.service';
import { PushNotificationComponent } from './component/push-notification.component';

const routes: Routes = Route.withShell([
  { path: '', redirectTo: '/push-notification', pathMatch: 'full' },
  { path: 'push-notification', component: PushNotificationComponent }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PushNotificationRoutingModule{}
