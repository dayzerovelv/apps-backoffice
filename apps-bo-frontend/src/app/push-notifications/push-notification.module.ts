import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { PushNotificationComponent } from './component/push-notification.component';
import { PushNotificationRoutingModule } from './push-notification-routing.module';
import { PushNotificationService } from './services/push-notification.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PushNotificationRoutingModule,
    SharedModule
  ],
  declarations: [PushNotificationComponent],
  providers: [PushNotificationService]
})
export class PushNotificationModule { }
