import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { UserService } from './services/user.service';

@NgModule({
  imports: [SharedModule],
  declarations: [],
  providers: [UserService]
})
export class UserModule {}
