import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { User } from '../interfaces/user';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  getUsersWithNotificationToken(): Observable<User[]> {
    return this.http.get(environment.userTokenURL)
      .map((response: Response) => this.parseUsers(response));
  }

  private parseUsers(json): User[] {
    let users: User[] = [];
    if (json.length > 0) {
      json.forEach(element => {
        users.push(User.fromJson(element));
      });
    }

    return users;
  }
}
