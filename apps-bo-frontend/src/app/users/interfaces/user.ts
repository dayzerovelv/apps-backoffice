export class User {
  id: string;
  name: string;
  surname: string;
  email: string;
  department: string;
  teamName: string;
  companyRole: string;
  status: string;
  notificationToken: string;
  fullname?: string;


  static fromJson(json): User {
    let user: User = new User();

    user.id = json['id'];
    user.name = json['name'];
    user.surname = json['surname'];
    user.email = json['email'];
    user.department = json['department'];
    user.teamName = json['teamName'];
    user.companyRole = json['companyRole'];
    user.status = json['status'];
    user.notificationToken = json['notificationToken'];

    user.fullname = user.name + ' ' + user.surname;

    return user;
  }

}
