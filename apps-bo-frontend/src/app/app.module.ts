import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { MainPageModule } from './shared/components/main-page/main-page.module';
import { SharedModule } from './shared/shared.module';
import { AuthenticationService } from './services/auth.service';
import { AuthenticationGuard } from './guards/auth.guard';
import { I18nModule } from './i18n/i18n.module';
import { ConfigModule } from './configuration/config.module';
import { PushNotificationModule } from './push-notifications/push-notification.module';
import { UserModule } from './users/user.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    LoginModule,
    MainPageModule,
    I18nModule,
    ConfigModule,
    PushNotificationModule,
    UserModule,
    AppRoutingModule // THIS MODULE ALWAYS HAS TO BE THE LAST!!!!
  ],
  providers: [AuthenticationService,
    AuthenticationGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
