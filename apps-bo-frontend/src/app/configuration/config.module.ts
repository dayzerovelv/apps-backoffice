import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { ConfigComponent } from './component/config.component';
import { ConfigRoutingModule } from './config-routing.module';
import { ConfigService } from './services/config.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ConfigRoutingModule,
    SharedModule
  ],
  declarations: [
    ConfigComponent
  ],
  providers: [
    ConfigService
  ]
})
export class ConfigModule { }
