import { Component, OnInit } from "@angular/core";
import { ConfigService } from "../services/config.service";
import { LoaderService } from "../../shared/services/loader.service";
import { Config } from "../interface/config";
import { MatSnackBar } from "@angular/material";
import { I18nService } from "../../i18n/services/i18n.service";
import { Language } from "../../i18n/interfaces/language";

@Component({
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {

  config: Config;
  /*langs = [
    { value: 'pt-pt', viewValue: 'Portuguese' },
    { value: 'pt-br', viewValue: 'Portuguese(Brazilian)' },
    { value: 'en-gb', viewValue: 'English(UK)' },
    { value: 'en-us', viewValue: 'English(United States)' }
  ];*/
  langs: Language[];

  constructor(private configService: ConfigService,
    private i18nService: I18nService,
    private loader: LoaderService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.config = new Config();
    this.loader.display(true);
    this.configService.getConfigs()
      .subscribe((config) => {
        this.config = config;
        this.loader.display(false);
      });
    this.i18nService.getSupportedLangs()
    .subscribe((langs) => {
      this.langs = langs;
      this.loader.display(false);
    })
  }

  save() {
    console.log(this.config);
    this.loader.display(true);
    this.configService.applyConfigs(this.config)
      .subscribe(() => {
        this.snackBar.open('Configurações aplicadas com sucesso', 'OK', { duration: 2000, });
        this.loader.display(false);
      });
  }
}
