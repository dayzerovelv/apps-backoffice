import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Route } from '../shared/route.service';
import { ConfigComponent } from './component/config.component';

const routes: Routes = Route.withShell([
  { path: '', redirectTo: '/configs', pathMatch: 'full' },
  { path: 'configs', component: ConfigComponent }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigRoutingModule{}
