export class Config {
  id: string;
  defaultLang: string;
  bgColor: string;
  dailyMood: boolean;
  categories: boolean;
  gamification: boolean;
  recursiveFeedback: boolean;

  constructor(){
    this.defaultLang = "pt-pt";
    this.bgColor = "#044d72";
    this.dailyMood = false;
    this.categories = false;
    this.gamification = false;
    this.recursiveFeedback = false;
  }

  static fromJson(jsonString): Config {
    const config: Config = new Config();

    config.id = jsonString['id'];
    config.defaultLang = jsonString['defaultLang'];
    config.bgColor = jsonString['bgColor'];
    config.dailyMood = jsonString['dailyMood'];
    config.categories = jsonString['categories'];
    config.gamification = jsonString['gamification'];
    config.recursiveFeedback = jsonString['recursiveFeedback'];

    return config;
  }


}
