import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Config } from '../interface/config';
import { environment } from '../../../environments/environment';
import { Response } from '@angular/http';

@Injectable()
export class ConfigService {

  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  getConfigs(): Observable<Config> {
    return this.http.get(environment.configURL)
      .map((response: Response) => Config.fromJson(response));
  }



  applyConfigs(config: Config) {
    return this.http.post(environment.configURL, JSON.stringify(config), { headers: this.headers });
  }
}
