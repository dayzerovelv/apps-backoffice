import { Component } from "@angular/core";
import { AuthenticationService } from "../services/auth.service";
import { Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";
import { environment } from "../../environments/environment";

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  creds;

  constructor(private authService: AuthenticationService,
    private router: Router,
    public snackBar: MatSnackBar) {
      this.creds = {};
    }

  login() {
    // Uncomment this line to bypass login
    // and don't forget to also uncomment in the guard
    this.router.navigate(['home']);
    /*this.authService.login(this.creds)
      .subscribe(
        () =>{
          this.router.navigate(['/home']);
        },
        (error => {
          this.snackBar.open('Utilizador ou password incorrectos', 'OK', {
            duration: 2000,
          });
        }));*/
  }
}
