import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Route } from '../shared/route.service';
import { I18nUploadComponent } from './fileupload/i18n-upload.component';

const routes: Routes = Route.withShell([
  { path: '', redirectTo: '/i18n-upload', pathMatch: 'full' },
  { path: 'i18n-upload', component: I18nUploadComponent }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class I18nRoutingModule{}
