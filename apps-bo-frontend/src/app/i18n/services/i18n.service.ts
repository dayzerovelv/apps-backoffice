import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { Language } from '../interfaces/language';

@Injectable()
export class I18nService {

  constructor(private http: HttpClient) {

  }

  uploadI18nFile(file: File, userId: string) {
    let formData = new FormData();
    formData.append('i18nFile', file);
    formData.append('userId', userId);

    return this.http.post(environment.i18nURL, formData);
  }

  getSupportedLangs(): Observable<Language[]> {
    return this.http.get(environment.supportedLangsURL)
      .map((response: Response) => this.parseSupportedLangs(response));
  }

  parseSupportedLangs(json): Language[] {
    let keys = Object.keys(json);
    let langs: Language[] = [];
    keys.forEach(key => {
      let lang = new Language();
      lang.locale = key;
      lang.description = json[key];
      langs.push(lang);
    });
    return langs;
  }
}
