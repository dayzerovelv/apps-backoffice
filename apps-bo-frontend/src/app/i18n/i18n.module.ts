import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { I18nRoutingModule } from './i18n-routing.module';
import { I18nUploadComponent } from './fileupload/i18n-upload.component';
import { I18nService } from './services/i18n.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    I18nRoutingModule,
    SharedModule
  ],
  declarations: [
    I18nUploadComponent
  ],
  providers: [
    I18nService
  ]
})
export class I18nModule { }
