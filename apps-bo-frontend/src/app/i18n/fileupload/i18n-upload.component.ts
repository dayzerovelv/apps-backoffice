import { Component } from "@angular/core";
import { I18nService } from "../services/i18n.service";
import { LoaderService } from "../../shared/services/loader.service";
import { MatSnackBar } from "@angular/material";

@Component({
  templateUrl: './i18n-upload.component.html',
  styleUrls: ['./i18n-upload.component.scss']
})
export class I18nUploadComponent {

  constructor(private i18nService: I18nService,
    private loader: LoaderService,
    private snackBar: MatSnackBar) { }

  onFileSelect(event) {
    const list: FileList = event.srcElement.files;
    if (list && list.length > 0) {
      let file: File = list.item(0);
      this.loader.display(true);
      this.i18nService.uploadI18nFile(file, "123")
        .subscribe(() => {
          this.snackBar.open('Ficheiro Carregado com Sucesso', 'OK', { duration: 2000, });
          this.loader.display(false);
        })
    }
  }
}
