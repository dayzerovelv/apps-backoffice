import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  DateAdapter,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSlideToggleModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { BaseComponent } from './base/base.component';
import { LoaderComponent } from './components/loader/loader.component';
import { MainFooterComponent } from './components/main-footer/main-footer.component';
import { MainHeaderComponent } from './components/main-header/main-header.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { RequestInterceptor } from './services/httpinterceptor';
import { ErrorHandler } from './services/error_handler';
import { LoaderService } from './services/loader.service';
import { ColorPickerModule } from 'ngx-color-picker';

/**
 * Module responsible for loading all common components
 * across the application
 */
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    RouterModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatExpansionModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    ColorPickerModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatExpansionModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    ColorPickerModule
  ],
  declarations: [
    MainFooterComponent,
    MainHeaderComponent,
    SideNavComponent,
    BaseComponent,
    LoaderComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    ErrorHandler,
    LoaderService
  ],
  entryComponents: [] // FOR MAT DIALOG COMPONENTS
})

export class SharedModule { }
