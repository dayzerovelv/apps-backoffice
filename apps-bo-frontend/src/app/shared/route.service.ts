import { Routes } from '@angular/router';

import { AuthenticationGuard } from '../guards/auth.guard';
import { BaseComponent } from './base/base.component';


/**
 * Provides helper methods to create routes.
 */
export class Route {

  /**
   * Creates routes using the shell component and authentication.
   * @param routes The routes to add.
   * @return {Routes} The new routes using shell as the base.
   */
  static withShell(routes: Routes): Routes {
    return [{
      path: '',
      component: BaseComponent,
      children: routes,
      canActivate: [AuthenticationGuard]
    }];
  }
}
