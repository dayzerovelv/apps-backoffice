import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../services/loader.service';

@Component({
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  isLoading: boolean = false;

  constructor(private loaderService: LoaderService){}

  ngOnInit() {
    // Subscribe to Loader service in order to show/hide loading spinner
    this.loaderService.status.subscribe(
      (val: boolean) => { this.isLoading = val; }
    );
  }
}
