import { Component } from "@angular/core";

/**
 * Component that defines the loading spinner
 */
@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {

    constructor() {
     }

}
