import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page.component';
import { Route } from '../../route.service';

/**
 * Module that handles navigation for the home page
 */
const routes: Routes = Route.withShell([
    { path: 'home', component: MainPageComponent }
]);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainPageRoutingModule{}
