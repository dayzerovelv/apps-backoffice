import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainPageRoutingModule } from "./main-page.routing.module";
import { SharedModule } from "../../shared.module";
import { MainPageComponent } from "./main-page.component";
import { HomeBannerComponent } from "../home-banner/home-banner.component";

/**
 * Module that loads all dependencies
 * required for the home page
 */
@NgModule({
    imports: [
        CommonModule,
        MainPageRoutingModule,
        SharedModule
    ],
    declarations: [
        MainPageComponent,
        HomeBannerComponent
    ]
})

export class MainPageModule{}
