import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from '../../../../environments/environment';
import { AuthenticationService } from '../../../services/auth.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  name: string;

  constructor(private router: Router,
    private authService: AuthenticationService) { }

  ngOnInit() {
    /*this.authService.getLoggedUser()
      .then((user: UserBO) => this.name = user.name)*/1
  }

  navigateTo(where: any) {
    if (where !== '') {
      this.router.navigate(['./', where]);
    } else {
      this.router.navigate(['./']);
    }
  }

  logout() {
    this.authService.logout().subscribe(() => this.navigateTo("login"));
  }
}
