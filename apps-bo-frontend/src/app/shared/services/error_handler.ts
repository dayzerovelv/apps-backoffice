import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { LoaderService } from './loader.service';

@Injectable()
export class ErrorHandler {

  constructor(
    public snackbar: MatSnackBar,
    private loader: LoaderService
  ) {}

  public handleError(err: any) {
    this.loader.display(false);
    console.log(err);
    this.snackbar.open(err.error , 'close', {
      duration: 2000,
    });
  }
}
