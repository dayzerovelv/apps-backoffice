import 'rxjs/add/observable/of';

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import { AuthenticationService } from '../services/auth.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private router: Router,
    private authService: AuthenticationService) { }

  canActivate() {
    // uncomment this line to bypass auth guard
    return Observable.of(true);
    /*return this.authService.isAuthenticated().then(
      (isAuthenticated) => {
        if (!isAuthenticated) {
          this.router.navigate(['/login'])
        }
        return isAuthenticated;
      }
    )*/
  }
}
