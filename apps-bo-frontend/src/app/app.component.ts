import { Component } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  logedIn: Boolean = false;
  sideNavToggle = true;
  spinnerOn = true;
  userProfile: string;

  menuItems = [];

  constructor(public dialog: MatDialog, private router: Router) {}

  async ngOnInit() {
  }

  toggleLogin() {
    if (!this.logedIn) {
//      const dialogRef = this.dialog.open(LoginComponent, {
//        width: '350px',
//        data: { element: null}
//      });

//      dialogRef.afterClosed().subscribe(result => {
//        if (result) {
//          this.checkLogin(result);
//        }
//      });
    }
  }

  checkLogin(result) {
    const _self = this;
    if (result.length === 2) {
      const username: string = result[0];
      const password: string = result[1];
    }
  }

  toggleSideNav() {
    this.sideNavToggle = this.sideNavToggle ? false : true;
  }

  closeMenu() {
    this.sideNavToggle = true;
  }

}
