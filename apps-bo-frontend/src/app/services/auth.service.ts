import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  login(creds){
    return this.http.post(environment.loginURL, JSON.stringify(creds), { headers: this.headers })
    .map(() => {});
  }

  logout(){
    return this.http.get(environment.logoutURL)
    .map(() => {});
  }

  isAuthenticated() {
    return this.http.get(environment.isAuthenticatedURL).toPromise()
      .then((resp) => {
        return resp.json().isAuthenticated;
      });
  }

  getLoggedUser() {
    return this.http.get(environment.currentUserURL).toPromise()
      .then((resp) => {
        return resp.json();
      });
  }

}
