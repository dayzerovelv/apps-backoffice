// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

const APP_BASE_URL = 'http://localhost:8080/apps-bo';
const APP_SERVICES_URL = APP_BASE_URL + '/api/myf';

const HEADER_APPLICATION_JSON = { 'Content-Type': 'application/json' };
const HEADER_APPLICATION_FORM_URL_ENCODED = { 'Content-Type': 'application/x-www-form-urlencoded' };

export const environment = {
  production: false,
  isDev: true,

  allowedActionPlanExt: 'pdf',
  allowedDocExt: ['pdf', 'ppt', 'pptx'],

  // headers definition
  headers: {
    applicationJson: HEADER_APPLICATION_JSON,
    formUrlEncoded: HEADER_APPLICATION_FORM_URL_ENCODED
  },

  /**
   * I18N
   */
  i18nURL: APP_SERVICES_URL + '/config/i18n',
  supportedLangsURL: APP_SERVICES_URL + '/config/i18n/langs',

  /**
   * CONFIGS
   */
  configURL: APP_SERVICES_URL + '/config',

  /**
   * BACKOFFICE AUTH
   */
  loginURL: APP_SERVICES_URL + '/login',
  isAuthenticatedURL: APP_SERVICES_URL + '/isAuthenticated',
  currentUserURL: APP_SERVICES_URL + '/loggedUser',
  logoutURL: APP_SERVICES_URL + '/logout',

  /**
   * USERS
   */
  userURL: APP_SERVICES_URL + '/user',
  userTokenURL: APP_SERVICES_URL + '/user/token',

  /**
   * PUSH MESSAGES
   */
  pushSingleURL: APP_SERVICES_URL + '/push/single',
  pushAllURL: APP_SERVICES_URL + '/push/all'



};
